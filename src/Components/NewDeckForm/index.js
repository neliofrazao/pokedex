import React from 'react'
import PropTypes from 'prop-types'
import { Button, Grid } from '@material-ui/core'
import { useForm } from 'react-hook-form'
import { InputField } from '../../Shared'
import Form from './NewDeckForm.styles'

const SearchForm = ({ onSubmit }) => {
  const { handleSubmit, register, errors } = useForm()

  return (
    <Form onSubmit={handleSubmit(onSubmit)} data-testid="data-search-form">
      <Grid container>
        <Grid item xs={12} lg={6}>
          <InputField
            label="Nome do deck"
            inputRef={register({ required: true })}
            name="deckName"
            data-testid="data-inptut-search"
            errors={errors}
          />
        </Grid>
        <Grid item xs={12} lg={6}>
          <Button
            type="submit"
            title="Nome do deck"
            data-testid="data-save"
            variant="contained"
            color="primary"
          >
            Salvar deck
          </Button>
        </Grid>
      </Grid>
    </Form>
  )
}

SearchForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
}

export default SearchForm
