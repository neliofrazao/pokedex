import React from 'react'
import PropTypes from 'prop-types'
import { ListItem, ListItemAvatar, Avatar, ListItemText } from '@material-ui/core'
import { Add } from '@material-ui/icons'

const ListCardItem = ({ hp, handleClick, name, supertype }) => (
  <ListItem button onClick={handleClick} data-testid={`data-list-${name}`}>
    <ListItemAvatar>
      <Avatar>
        <Add />
      </Avatar>
    </ListItemAvatar>
    <ListItemText primary={name} secondary={`hp: ${hp} | supertype: ${supertype}`} />
  </ListItem>
)

ListCardItem.propTypes = {
  hp: PropTypes.string,
  handleClick: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  supertype: PropTypes.string.isRequired,
}

ListCardItem.defaultProps = {
  hp: 'no HP',
}

export default ListCardItem
