import styled from 'styled-components'
import color from '../../utils/Theme/colors'

const Header = styled.div`
  padding: 1em;
  width: 100%;
  border-bottom: 1px solid ${color.graphite};
  margin-bottom: 1em;
  color: ${color.graphite};
`
export default Header
