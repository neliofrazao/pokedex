import React, { useCallback } from 'react'
import PropTypes from 'prop-types'
import { Grid, Typography } from '@material-ui/core'
import Header from './DeckHeader.styles'

const CARDS_SUPERTYPES = {
  pokemon: 'Pokémon',
  traine: 'Trainer',
}
const DeckHeader = ({ data }) => {
  const countCards = useCallback(
    (type) => {
      return (data.cards && data.cards.filter((card) => card.supertype === type)) || []
    },
    [data.cards],
  )
  const traineCards = countCards(CARDS_SUPERTYPES.traine).length
  const pokerCards = countCards(CARDS_SUPERTYPES.pokemon).length

  return (
    <Header>
      <Grid container spacing={3}>
        <Grid item xs={6}>
          <Typography variant="h3" gutterBottom>
            {data.deckname}
          </Typography>
        </Grid>
        <Grid item xs={6}>
          <Typography variant="subtitle1" gutterBottom>
            cartas de treinador: {traineCards}
          </Typography>
          <Typography variant="subtitle1" gutterBottom>
            cartas de Pokemon: {pokerCards}
          </Typography>
        </Grid>
      </Grid>
    </Header>
  )
}

DeckHeader.propTypes = {
  data: PropTypes.oneOfType([PropTypes.object, PropTypes.arrayOf(PropTypes.object)]).isRequired,
}

export default DeckHeader
