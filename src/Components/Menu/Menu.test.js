import React from 'react'
import { render, screen } from '@testing-library/react'
import { injectRouter } from '../../helpers/testHelpers'
import { Menu, MenuItem } from './index'

const BaseRender = () => (
  <Menu>
    <MenuItem link="/deck" title="item menu">
      Home
    </MenuItem>
    <MenuItem link="/new-deck" title="item menu">
      Novo deck
    </MenuItem>
  </Menu>
)

describe('Menu()', () => {
  test('should render menu component ', () => {
    const { getByTestId } = render(injectRouter(<BaseRender />))
    expect(getByTestId('data-menu')).toBeDefined()
  })

  test('should render menuItem component with two items ', () => {
    render(injectRouter(<BaseRender />))
    const menuItems = screen.getAllByRole('button').length

    expect(menuItems).toBe(2)
  })
})
