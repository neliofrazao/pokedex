import React from 'react'
import { render, screen } from '@testing-library/react'
import { injectRouter } from '../../helpers/testHelpers'
import Card from './index'

const BaseRender = () => <Card name="char" link="/link" data-testid="deck-info" />

describe('Card()', () => {
  test('should show link with right value ', () => {
    render(injectRouter(<BaseRender />))
    screen.getAllByRole('button')
  })
})
