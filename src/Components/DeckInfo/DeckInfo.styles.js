import styled from 'styled-components'
import { Card } from '@material-ui/core'
import color from '../../utils/Theme/colors'

const Deck = styled(Card)`
  & .MuiButtonBase-root {
    color: #fff;
    padding: 1.2em 1em;
  }
  & .button-deck {
    background: ${color.graphite};
    &:hover {
      background: ${color.black};
    }
  }
`
export default Deck
