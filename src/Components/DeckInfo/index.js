import React from 'react'
import PropTypes from 'prop-types'
import { CardActions, CardContent, Typography, Button } from '@material-ui/core'
import { DeleteForever } from '@material-ui/icons'
import { ButtonLink } from '../../Shared'
import Deck from './DeckInfo.styles'

const DeckInfo = ({ countCards, name, link, handleClick }) => (
  <Deck>
    <CardContent>
      <Typography gutterBottom variant="h5" component="h2">
        {name}
      </Typography>
      <Typography variant="subtitle2" gutterBottom>
        Total de cartas: {countCards}
      </Typography>
    </CardContent>
    <CardActions>
      <Button variant="contained" color="secondary" onClick={handleClick}>
        <DeleteForever />
        Apagar Deck
      </Button>
      <ButtonLink link={link} title={name} className="button-deck">
        Ver Deck
      </ButtonLink>
    </CardActions>
  </Deck>
)

DeckInfo.propTypes = {
  countCards: PropTypes.number.isRequired,
  handleClick: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  link: PropTypes.string.isRequired,
}

export default DeckInfo
