import React from 'react'
import { Router } from 'react-router-dom'
import { createMemoryHistory } from 'history'
import { SnackbarProvider } from 'notistack'

export const injectRouter = (
  Component,
  { route = '/', history = createMemoryHistory({ initialEntries: [route] }) } = {},
) => <Router history={history}>{Component}</Router>

export const injectTheme = (ui) => <SnackbarProvider>{ui}</SnackbarProvider>
