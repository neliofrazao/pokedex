import React, { Fragment, useCallback, useContext, useEffect, useState } from 'react'
import { Grid } from '@material-ui/core'
import { LoadContext } from '../../Shared'
import { DeckInfo } from '../../Components'
import { getLocalStorage, saveLocalStorage } from '../../utils/localStorage'

const DeckList = () => {
  const [deckList, setDeckList] = useState([])
  const { isLoad, setIsLoad } = useContext(LoadContext)

  const getDeckData = useCallback(async () => {
    setIsLoad(true)
    try {
      const data = getLocalStorage('deck')
      setDeckList(data)
    } catch (error) {
      setDeckList([])
    }
    setIsLoad(false)
  }, [setIsLoad])

  const handleDeleteDeck = useCallback(
    (cardId) => {
      const getCurrentDeck = [...deckList]
      const removeDeck = getCurrentDeck.filter((card, index) => index !== cardId)
      saveLocalStorage('deck', removeDeck)
      getDeckData()
    },
    [deckList, getDeckData],
  )

  useEffect(() => {
    getDeckData()
  }, [getDeckData, setIsLoad])

  return (
    <div data-testid="data-deck-list">
      <Grid container spacing={3}>
        {!isLoad && (
          <>
            {deckList ? (
              deckList.map((deck, index) => (
                // eslint-disable-next-line react/no-array-index-key
                <Fragment key={index}>
                  <Grid item xs={12} md={4} lg={3}>
                    <DeckInfo
                      countCards={deck.cards.length}
                      name={deck.deckname}
                      link={`/deck/${index}`}
                      handleClick={() => handleDeleteDeck(index)}
                    />
                  </Grid>
                </Fragment>
              ))
            ) : (
              <p>Não tem deck cadastrado</p>
            )}
          </>
        )}
      </Grid>
    </div>
  )
}

export default DeckList
