import React from 'react'
import { render } from '@testing-library/react'
import DeckList from './index'

describe('DeckList()', () => {
  test('should render DeckList component ', () => {
    const { getByTestId } = render(<DeckList />)
    expect(getByTestId('data-deck-list')).toBeDefined()
  })
})
