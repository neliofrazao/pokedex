import React, { Fragment, useContext, useEffect, useState } from 'react'
import RouterPropTypes from 'react-router-prop-types'
import { Button, Grid, List } from '@material-ui/core'
import { ListCardItem, NewDeckForm } from '../../Components'
import { LoadContext, Card } from '../../Shared'
import useHandleCard from './useHandleCard'
import useAlert from '../../utils/Snackbar/useSnackbar'
import { getLocalStorage, saveLocalStorage } from '../../utils/localStorage'
import api from '../../api/cards/cards'

const NewDeck = ({ history }) => {
  const [pokemonCards, setPokemonCards] = useState([])
  const { isLoad, setIsLoad } = useContext(LoadContext)
  const { newDeck, handleAddCard, handleDeleteCard } = useHandleCard(pokemonCards)
  const { showError, showSuccess } = useAlert()

  const onSubmit = async ({ deckName }) => {
    const deckSize = newDeck.length
    const formatDeck = {
      deckname: deckName,
      cards: newDeck,
    }

    const deckStorage = getLocalStorage('deck')
    deckStorage.push(formatDeck)
    if (deckSize < 25)
      showError(`O seu deck deve ter no mínimo 24 cartas, você já selecionou ${deckSize}.`)
    else {
      saveLocalStorage('deck', deckStorage)
      showSuccess(`O deck ${deckName} foi salvo com sucesso.`)
      history.push('/')
    }
  }

  useEffect(() => {
    const getPokemonData = async () => {
      setIsLoad(true)
      try {
        const { cards } = await api.getPokemonCards()
        setPokemonCards(cards)
      } catch (error) {
        showError('Tivemos um comportamento inesperado, por favor tente mais tarde')
        setPokemonCards([])
      }
      setIsLoad(false)
    }
    getPokemonData()
  }, [setIsLoad, showError])

  return (
    <div data-testid="data-new-deck">
      <>
        <Grid container direction="row" spacing={3}>
          {!isLoad && (
            <>
              <Grid container>
                <Grid item xs={12} lg={6}>
                  <NewDeckForm onSubmit={onSubmit} />
                </Grid>
              </Grid>
              <Grid item xs={9}>
                <Grid container spacing={3}>
                  {newDeck &&
                    newDeck.map(({ id, imageUrl, name }) => (
                      <Fragment key={id}>
                        <Grid item xs={12} md={4} lg={2}>
                          <Card cardName={name} cardImage={imageUrl}>
                            <Button
                              variant="contained"
                              color="secondary"
                              onClick={() => handleDeleteCard(id)}
                            >
                              Remover
                            </Button>
                          </Card>
                        </Grid>
                      </Fragment>
                    ))}
                </Grid>
              </Grid>
              <Grid item xs={3}>
                <List>
                  {pokemonCards &&
                    pokemonCards.length > 1 &&
                    pokemonCards.map(({ id, name, supertype, hp }) => (
                      <Fragment key={id}>
                        <ListCardItem
                          name={name}
                          supertype={supertype}
                          hp={hp}
                          handleClick={() => handleAddCard(id, name)}
                        />
                      </Fragment>
                    ))}
                </List>
              </Grid>
            </>
          )}
        </Grid>
      </>
    </div>
  )
}

NewDeck.propTypes = {
  history: RouterPropTypes.history.isRequired,
}

export default NewDeck
