import { useCallback, useState } from 'react'
import useAlert from '../../utils/Snackbar/useSnackbar'

const useHandleCard = (pokemonCards) => {
  const [newDeck, setNewDeck] = useState([])
  const { showInfo } = useAlert()
  const handleAddCard = useCallback(
    (cardId, name) => {
      const getCard = pokemonCards.filter((card) => card.id === cardId)
      const getName = newDeck.filter((card) => card.name === name)
      const getCardWithSameId = newDeck.filter((card) => card.id === cardId)
      if (getName.length >= 4) showInfo('O Número máximo de cartas com o mesmo nome é 4')
      else if (getCardWithSameId.length) showInfo('Você já selecionou essa carta')
      else if (newDeck.length >= 60) showInfo('O seu deck pode ter no máximo 60 cartas')
      else setNewDeck((oldDeck) => [...oldDeck, ...getCard])
    },
    [pokemonCards, newDeck, showInfo],
  )

  const handleDeleteCard = useCallback(
    (cardId) => {
      const getCurrentDeck = [...newDeck]
      const removeCard = getCurrentDeck.filter((card) => card.id !== cardId)
      setNewDeck(removeCard)
    },
    [newDeck],
  )

  return { newDeck, handleAddCard, handleDeleteCard }
}

export default useHandleCard
