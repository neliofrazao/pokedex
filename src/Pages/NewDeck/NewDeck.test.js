import React from 'react'
import { render, waitForElement } from '@testing-library/react'
import { createMemoryHistory } from 'history'
import NewDeck from './index'
import { injectTheme } from '../../helpers/testHelpers'
import api from '../../api/cards/cards'

const pokerData = {
  cards: {
    id: 'ex6-55',
    name: 'Bulbasaur',
    nationalPokedexNumber: 1,
    imageUrl: 'https://images.pokemontcg.io/ex6/55.png',
    imageUrlHiRes: 'https://images.pokemontcg.io/ex6/55_hires.png',
    types: ['Grass'],
    supertype: 'Pokémon',
    subtype: 'Basic',
    hp: '50',
    retreatCost: ['Colorless'],
    convertedRetreatCost: 1,
    number: '55',
    artist: 'Hajime Kusajima',
    rarity: 'Common',
    series: 'EX',
    set: 'FireRed & LeafGreen',
    setCode: 'ex6',
    attacks: [
      {
        cost: ['Colorless'],
        name: 'Ram',
        text: '',
        damage: '10',
        convertedEnergyCost: 1,
      },
      {
        cost: ['Grass', 'Colorless'],
        name: 'Gouge',
        text: 'Flip a coin. If heads, this attack does 20 damage plus 10 more damage.',
        damage: '20+',
        convertedEnergyCost: 2,
      },
    ],
    weaknesses: [
      {
        type: 'Psychic',
        value: '×2',
      },
    ],
  },
}

const props = {
  history: createMemoryHistory('/'),
}

describe('NewDeck()', () => {
  test('should render hero list component ', async () => {
    const { getByTestId } = render(injectTheme(<NewDeck {...props} />))
    await waitForElement(() => getByTestId('data-new-deck'))
    expect(getByTestId('data-new-deck')).toBeDefined()
  })

  test('should api return with right', async () => {
    api.getPokemonCards = jest.fn().mockReturnValueOnce(pokerData)
    const { getByTestId } = render(injectTheme(<NewDeck {...props} />))
    await waitForElement(() => getByTestId('data-new-deck'))
    expect(api.getPokemonCards).toHaveReturnedWith(pokerData)
    expect(api.getPokemonCards).toHaveBeenCalledTimes(1)
  })

  test('should api return with right', async () => {
    api.getPokemonCards = jest.fn().mockReturnValueOnce(pokerData)
    const { getByTestId } = render(injectTheme(<NewDeck {...props} />))

    await waitForElement(() => getByTestId('data-new-deck'))
    expect(api.getPokemonCards).toHaveReturnedWith(pokerData)
    expect(api.getPokemonCards).toHaveBeenCalledTimes(1)
  })

  test('should receive error2', async () => {
    api.getPokemonCards = jest.fn().mockRejectedValueOnce({})
    const { getByTestId, getByText } = render(injectTheme(<NewDeck {...props} />))
    const ERROR_TEST = 'Tivemos um comportamento inesperado, por favor tente mais tarde'
    await waitForElement(() => getByTestId('data-new-deck'))
    const SUT = getByText(ERROR_TEST).innerHTML
    expect(SUT).toContain(ERROR_TEST)
    expect(api.getPokemonCards).toHaveBeenCalledTimes(1)
  })
})
