import React, { Fragment, useContext, useEffect, useState } from 'react'
import RouterPropTypes from 'react-router-prop-types'
import { Grid } from '@material-ui/core'
import { LoadContext, Card } from '../../Shared'
import { DeckHeader } from '../../Components'
import useAlert from '../../utils/Snackbar/useSnackbar'
import { getLocalStorage } from '../../utils/localStorage'

const DeckDetail = ({ match }) => {
  const [pokemonCards, setPokemonCards] = useState([])
  const { isLoad, setIsLoad } = useContext(LoadContext)
  const { showError } = useAlert()
  const { deckId } = match.params

  useEffect(() => {
    const getPokemonData = async () => {
      setIsLoad(true)
      try {
        const data = getLocalStorage('deck')
        setPokemonCards(data[deckId])
      } catch (error) {
        setPokemonCards([])
      }
      setIsLoad(false)
    }
    getPokemonData()
  }, [deckId, setIsLoad, showError])

  return (
    <div data-testid="data-new-deck">
      <>
        <Grid container direction="row" spacing={3}>
          {!isLoad && (
            <>
              <DeckHeader data={pokemonCards} />
              <Grid container spacing={3}>
                {pokemonCards.cards &&
                  pokemonCards.cards.map(({ id, imageUrl, name }) => (
                    <Fragment key={id}>
                      <Grid item xs={12} md={4} lg={2}>
                        <Card cardName={name} cardImage={imageUrl} />
                      </Grid>
                    </Fragment>
                  ))}
              </Grid>
            </>
          )}
        </Grid>
      </>
    </div>
  )
}

DeckDetail.propTypes = {
  match: RouterPropTypes.match.isRequired,
}

export default DeckDetail
