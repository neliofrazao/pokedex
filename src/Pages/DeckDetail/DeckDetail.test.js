import React from 'react'
import { render } from '@testing-library/react'
import { injectTheme } from '../../helpers/testHelpers'
import DeckDetail from './index'

const props = {
  match: {
    params: { heroId: '10' },
    path: '',
    url: '',
  },
}
const values = {
  deckname: 'Poke2',
  cards: [
    {
      convertedRetreatCost: 1,
      evolvesFrom: 'Gloom',
      hp: '120',
      id: 'xy7-4',
      imageUrl: 'https://images.pokemontcg.io/xy7/4.png',
      imageUrlHiRes: 'https://images.pokemontcg.io/xy7/4_hires.png',
      name: 'Bellossom',
      retreatCost: ['Colorless'],
      set: 'Ancient Origins',
      supertype: 'Pokémon',
      types: ['Grass'],
    },
  ],
}

window.localStorage.setItem('deck', values)

jest.spyOn(window, 'fetch').mockImplementationOnce(() => {
  return Promise.resolve({
    json: () => Promise.resolve({ deck: values }),
  })
})

describe('DeckDetail()', () => {
  test('should render DeckDetail component', () => {
    render(injectTheme(<DeckDetail {...props} />))
  })
})
