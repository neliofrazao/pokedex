import http from '../../utils/http'

const getPokemonCards = async () => {
  const { data } = await http.get('/cards')
  return data
}

export default {
  getPokemonCards,
}
