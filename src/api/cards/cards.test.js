import http from '../../utils/http'
import api from './cards'

describe('PokemonCards()', () => {
  test('should GET pokemonsCards url with right param', async () => {
    jest.spyOn(http, 'get')
    http.get.mockImplementation((url) => ({
      data: {
        url,
      },
    }))

    const result = await api.getPokemonCards()
    expect(result.url).toBe('/cards')
    http.get.mockRestore()
  })
})
