export const saveLocalStorage = (item, value) => localStorage.setItem(item, JSON.stringify(value))
export const getLocalStorage = (item) => {
  const deckStorage = JSON.parse(localStorage.getItem(item)) || []
  return deckStorage
}
