import { useCallback } from 'react'
import { useSnackbar } from 'notistack'

const useAlert = () => {
  const { enqueueSnackbar } = useSnackbar()
  const showMessage = useCallback(
    (message) => {
      enqueueSnackbar(message)
    },
    [enqueueSnackbar],
  )
  const showError = useCallback(
    (message) => {
      enqueueSnackbar(message, { variant: 'error' })
    },
    [enqueueSnackbar],
  )
  const showInfo = useCallback(
    (message) => {
      enqueueSnackbar(message, { variant: 'info' })
    },
    [enqueueSnackbar],
  )
  const showWarning = useCallback(
    (message) => {
      enqueueSnackbar(message, { variant: 'warning' })
    },
    [enqueueSnackbar],
  )
  const showSuccess = useCallback(
    (message) => {
      enqueueSnackbar(message, { variant: 'success' })
    },
    [enqueueSnackbar],
  )

  return { showMessage, showError, showInfo, showWarning, showSuccess }
}

export default useAlert
