import React, { useState, useMemo, Suspense, lazy } from 'react'
import { BrowserRouter as Router, Switch, Route, withRouter } from 'react-router-dom'
import { SnackbarProvider } from 'notistack'
import Layout from './utils/Layout'
import RouteLoader from './utils/RouteLoader'
import LoadContext from './Shared/Loading/store'

const DeckList = withRouter(lazy(() => import('./Pages/DeckList')))
const DeckDetail = withRouter(lazy(() => import('./Pages/DeckDetail')))
const NewDeck = withRouter(lazy(() => import('./Pages/NewDeck')))

const Routes = () => {
  const [isLoad, setIsLoad] = useState(false)
  const provideLoad = useMemo(() => ({ isLoad, setIsLoad }), [isLoad, setIsLoad])

  return (
    <Router>
      <Suspense fallback={<RouteLoader />}>
        <SnackbarProvider maxSnack={3}>
          <LoadContext.Provider value={provideLoad}>
            <Layout>
              <Switch>
                <Route path="/" exact component={DeckList} />
                <Route path="/deck/:deckId" exact component={DeckDetail} />
                <Route path="/new-deck" exact component={NewDeck} />
              </Switch>
            </Layout>
          </LoadContext.Provider>
        </SnackbarProvider>
      </Suspense>
    </Router>
  )
}

export default Routes
