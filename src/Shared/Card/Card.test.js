import React from 'react'
import { render } from '@testing-library/react'
import { injectRouter } from '../../helpers/testHelpers'
import Card from './index'

const BaseRender = () => (
  <Card cardName="card name" cardImage="/image">
    <p data-testid="data-html-elemnt">Some text</p>
  </Card>
)

describe('Card()', () => {
  test('should print img with right values ', () => {
    const { getByTitle, getByAltText } = render(injectRouter(<BaseRender />))

    expect(getByTitle('card name')).toBeDefined()
    expect(getByAltText('card name')).toBeDefined()
  })

  test('should render children props', () => {
    const { getByTestId } = render(injectRouter(<BaseRender />))
    const SUT = getByTestId('data-html-elemnt').innerHTML

    expect(SUT).toBe('Some text')
  })
})
