import React from 'react'
import PropTypes from 'prop-types'
import CardContainer from './Card.styles'

const Card = ({ cardImage, cardName, children }) => (
  <CardContainer>
    <figure>
      <img src={cardImage} alt={cardName} title={cardName} />
    </figure>
    <div>{children}</div>
  </CardContainer>
)

Card.propTypes = {
  cardImage: PropTypes.string.isRequired,
  cardName: PropTypes.string.isRequired,
  children: PropTypes.element,
}

Card.defaultProps = {
  children: null,
}

export default Card
