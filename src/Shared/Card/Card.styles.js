import styled from 'styled-components'
import color from '../../utils/Theme/colors'

const CardContainer = styled.div`
  text-align: center;
  & img {
    width: 100%;
    max-width: 250px;
  }
  & figure {
    padding: 0;
    margin: 0;
  }
  & .MuiButtonBase-root {
    color: #fff;
    width: 100%;
    max-width: 250px;
    padding: 1.2em 1em;
  }
  & .MuiButton-containedPrimary {
    background: ${color.graphite};
    &:hover {
      background: ${color.black};
    }
  }
`
export default CardContainer
